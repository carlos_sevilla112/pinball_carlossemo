﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour {

    public Transform Ball;
    public Transform Respawner;
    public Transform DeathSpawn;
    public float tries;
    bool stillAlive = true ;



    void OnTriggerEnter(Collider other)
    {
       
        tries -=1;

        if(tries == 0)
        {
            stillAlive = false;
        }

        else if (stillAlive == true)
        {
            if (other.tag == "Ball")
            {
                Ball.transform.position = Respawner.transform.position;
            }
        }
        else if (stillAlive == false)
        {
            Ball.transform.position = DeathSpawn.transform.position;
        }
      

    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
